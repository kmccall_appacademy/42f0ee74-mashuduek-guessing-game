# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

require 'byebug'
def guessing_game
  number = rand(100) + 1
  guesses = 1
  puts 'Try to guess a number.'
  guess = gets.chomp.to_i
  until guess == number
    puts ' Guess.'
    print "#{guess}, thats a good guess."
    print " You've taken #{guesses} guesses so far."
    print ' Your number was too high.' if guess > number
    print ' Your number was too low.' if guess < number
    guesses += 1
    guess = gets.chomp.to_i
  end
  print "YES! #{number}! In #{guesses} tries... Impressive!" if guess == number
end
