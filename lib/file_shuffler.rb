# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def file_shuffler
  f = gets.chomp
  new_shuffled = (File.readlines(f)).shuffle
  shuffled = File.new("#{f}-shuffled.txt", "w+")
  shuffled.write(new_shuffled)
  shuffled.close
end

p file_shuffler
